from flask import Flask
import logging

social_app = Flask(__name__, instance_relative_config=True)
social_app.config.from_pyfile('application.cfg')
#do logging
file_handler = logging.FileHandler('app.log')
social_app.logger.addHandler(file_handler)
social_app.logger.setLevel(logging.INFO)


from social_app import views
