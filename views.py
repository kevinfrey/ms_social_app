#!/usr/bin/python
import flask
from flask import render_template
from flask import request
from social_app import social_app
import time
import twitter
import requests
from instagram.client import InstagramAPI


#Configurations:


# Variables that contains the user credentials to access Twitter API
api = twitter.Api(consumer_key=social_app.config['CONSUMER_KEY'],
                  consumer_secret=social_app.config['CONSUMER_SECRET'],
                  access_token_key=social_app.config['ACCESS_TOKEN_KEY'],
                  access_token_secret=social_app.config['ACCESS_TOKEN_SECRET'])

#Fix this not dynamic right now, but would be easy to script out
#instagram_access_token = '31225359.223329e.9c71486c07d0449ea27d6b70ca18e49d'
ig_api =InstagramAPI(client_secret=social_app.config['IG_CLIENT_SECRET'],
                   access_token=social_app.config['IG_ACCESS_TOKEN'])


@social_app.route('/')
def index():
	term = "mobileserve"
	start= time.clock()
	total_results = []
	twitter_results = api.GetSearch(term=term)
	ig_result = ig_api.tag_recent_media(tag_name=term)
	media = ig_result[0]

	#Weird right now you can't get back results from IG that I would expect.  Only []
	print media

	total_results.append(total_results)
	total_results.append(ig_result)

	end = time.clock()
	count = len(twitter_results)
	timing = (end-start)*1000
	return render_template('index.html', results=twitter_results, count=count, timing=timing, term=term, page=1)


@social_app.route('/search')
def search():
	term = request.args.get('term', '')
	# filters = request.args.get('filter', '')
	page = request.args.get('page', '')

	if not term or term == "null":
		term = "#mobileserve"

	start= time.clock()
	results = api.GetSearch(term=term, count=100)
	end = time.clock()
	count = len(results)
	timing = (end-start)*1000

	return render_template('index.html', results=results, count=count, timing=timing, term=term, page=page)
